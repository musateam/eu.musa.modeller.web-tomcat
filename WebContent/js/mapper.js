function mapCombo(object, Key1, Key2){
	var retorno=[];	
	for (var i=0; i<object.length; i++){
		var obj={};
		obj.value= object [i][Key1] + '.camel';
		obj.text= object [i][Key1] + '-' + object [i][Key2];
		retorno[i] = obj;
	}
	return retorno;
}

function mapCombo(object, Key1, Key2, key3){
	var retorno=[];	
	for (var i=0; i<object.length; i++){
		var obj={};
		//obj.value= object [i][Key1] + '.camel';
		obj.value= object [i][Key1] + '$' + object [i][key3];
		obj.text= object [i][Key2];
		retorno[i] = obj;
	}
	return retorno;
}

function mapComboComponents(object, Key1, Key2, key3){
	var retorno=[];	
	for (var i=0; i<object.length; i++){
		var obj={};		
		obj.value= object [i][Key1] + '$' + object [i][Key2];
		obj.text= object [i][key3];
		retorno[i] = obj;
	}
	return retorno;
}

function mapComboErrors(object, Key1, Key2, Key3){
	var retorno=[];
	// ningun error...
	if (object == undefined) {
		var obj={};
		obj.value= -1;
		obj.text= ' ';
		retorno[0] = obj;	
		return retorno;
	}

	// solo 1 error...
	if (object.length == undefined) {
		var obj={};
		obj.value= object[Key1];
		//obj.text= object [Key3];
		obj.text= object [Key1] + ':' + object [Key2] + ':' + object [Key3];
		retorno[0] = obj;
		return retorno;
	}
	
	// mas errores...
	for (var i=0; i<object.length; i++){
		var obj={};
		obj.value= object [i][Key1];
		//obj.text= object [i][Key3];
		obj.text= object [i][Key1] + ':' + object [i][Key2] + ':' + object [i][Key3];		
		retorno[i] = obj;
	}
	return retorno;
}


function mapCombov0(object, textKey, valueKey){
	var retorno=[];
	console.log(object);
	for (var i=0; i<object.length; i++){
		var obj={};
		obj.value= object [i][valueKey];
		obj.text= object [i][textKey];
		retorno[i] = obj;
	}
	return retorno;
}