function getAgents(url, id) {
	$.ajax({
		type : "GET",
		url : url,
		dataType : "json",
		success : function(data) {
			console.log("getComboData init");
			fillCombo(mapCombo(data.Agent, "id","name","description"), id);
			console.log("getComboData end");
		},
		error : function(jqXHR, textStatus, errorThrown, error) {
			console.log('error getApplicationList...');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log(error);
		}
	});
}

function getComponents(url, id) {
	$.ajax({
		type : "GET",
		url : url,
		dataType : "json",
		success : function(data) {
			console.log("getComboData init");
			fillCombo(mapComboComponents(data.Component, "appId","componentId","componentName"), id);
			console.log("getComboData end");
		},
		error : function(jqXHR, textStatus, errorThrown, error) {
			console.log('error getApplicationList...');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log(error);
		}
	});
}

function getValidationErrors(url, id) {
	$.ajax({
		type : "GET",
		url : url,
		dataType : "json",
		success : function(data) {
			console.log("getValidationErrors init");
			if(data != null && data.ValidationError != null) { fillCombo(mapComboErrors(data.ValidationError, "line", "type", "description"), id); }
			else
			{   //MCP para que la lista tenga el tamanno adecuado
				var obj={};
				obj.value= -1;
				var blanks = '&nbsp;'
				for (var i=0;i<80;i++) { blanks = blanks + '&nbsp;'}
				obj.text= blanks;
				var retorno=[];
				retorno[0] = obj;	
				fillCombo(retorno, id);
			}
			//October 2017
			$("#saveWindowMessage").fadeIn();
			console.log("getValidationErrors end");
		},
		error : function(jqXHR, textStatus, errorThrown, error) {
			console.log('error getValidationErrors...');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log(error);
		}
	});
}

function fillCombo(options, id) {
	contenido='';	
	for (var i = 0; i < options.length; i++) {		
		contenido += '<option value="' + options[i].value + '">'
				+ options[i].text + '</option>';

	}

	document.getElementById(id).innerHTML = contenido;
	var selectedValue = $("#agentsList").val();					
	if (selectedValue != undefined) { //chequeo introducido por MCP
		var agentDescription = selectedValue.substr(selectedValue.indexOf("$") + 1);									
		document.getElementById("agentDescription").innerHTML = agentDescription;
	 }
}

