define(["ace/lib/oop", "ace/mode/text", "ace/mode/text_highlight_rules"], function(oop, mText, mTextHighlightRules) {
	var HighlightRules = function() {
		var keywords = "64os|ABSTRACT|ALL|AND|ANY|BINARY|BOTH_MATCH|BPM|BYTES|BYTES_PER_SECOND|BooleanType|CORES|COUNT|CRITICAL|DAYS|DERIVATIVE|DIV|DOLLARS|DoubleType|E|EUROS|EVERY|EXACT|FATAL|FIRST_MATCH|FIXED|FIXED_DELAY|FIXED_RATE|FloatType|GIGABYTES|HIGH|HOURS|INTERVAL|IaaS|IntType|KILOBYTES|LOCAL|LOW|MAP|MAX|MEAN|MEASURABLE|MEASUREMENTS_ONLY|MEDIAN|MEDIUM|MEGABYTES|MILLISECONDS|MIN|MINUS|MINUTES|MODE|MODULO|MONTHS|NOT|N_ARY|OR|PERCENTAGE|PERCENTILE|PLUS|POUNDS|PRECEDES|PaaS|Public|REDUCE|REMOTE|REPEAT|REPEAT_UNTIL|REQUESTS|REQUESTS_PER_SECOND|SCC|SECONDS|SINGLE_EVENT|SLIDING|SOME|STD|SUCCESS|SaaS|StringType|TIMES|TIME_ONLY|TRANSACTIONS|TRANSACTIONS_PER_SECOND|TREE|UNARY|WARNING|WEEKS|WHEN|WITHIN|WITHIN_MAX|XOR|add|all|alternative|assignable|certifiable|divide|e|entity|false|importURI|included|instance|mandatory|multiply|product|push|qualitative|relative|remove|select|true|variable|violated|violation";
		this.$rules = {
			"start": [
				{token: "comment", regex: "\\/\\/.*$"},
				{token: "comment", regex: "\\/\\*", next : "comment"},
				{token: "string", regex: '["](?:(?:\\\\.)|(?:[^"\\\\]))*?["]'},
				{token: "string", regex: "['](?:(?:\\\\.)|(?:[^'\\\\]))*?[']"},
				{token: "constant.numeric", regex: "[+-]?\\d+(?:(?:\\.\\d*)?(?:[eE][+-]?\\d+)?)?\\b"},
				{token: "lparen", regex: "[\\[({]"},
				{token: "rparen", regex: "[\\])}]"},
				{token: "keyword", regex: "\\b(?:" + keywords + ")\\b"}
			],
			"comment": [
				{token: "comment", regex: ".*?\\*\\/", next : "start"},
				{token: "comment", regex: ".+"}
			]
		};
	};
	oop.inherits(HighlightRules, mTextHighlightRules.TextHighlightRules);
	
	var Mode = function() {
		this.HighlightRules = HighlightRules;
	};
	oop.inherits(Mode, mText.Mode);
	Mode.prototype.$id = "xtext/camel";
	Mode.prototype.getCompletions = function(state, session, pos, prefix) {
		return [];
	}
	
	return {
		Mode: Mode
	};
});
