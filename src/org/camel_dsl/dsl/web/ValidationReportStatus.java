package org.camel_dsl.dsl.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import eu.musa.modeller.utils.MCApp;
import eu.musa.modeller.utils.ManageMCApp;

//MCP
@WebServlet(name = "ValidationReportStatus", urlPatterns = "/ValidationReportStatus")
@SuppressWarnings("all")
public class ValidationReportStatus extends HttpServlet {
	private static SessionFactory factory;
	private static int Cycle = 5;

	public ValidationReportStatus() {
		super();
		System.out.println("ValidationReportStatus: init");

		try {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
					// .configure("/info/java/tips/config/hibernate.cfg.xml").build();
					.configure("eu/musa/modeller/config/hibernate.cfg.xml").build();
			factory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

			// factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("ValidationReportStatus(): Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		System.out.println("ValidationReportStatus: end");
	}

	private String retrieveMCAppId(HttpServletRequest req) {
		String res = null;
		int id = -1;
		String pathInfo = req.getPathInfo();
		if (pathInfo != null && pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
			id = Integer.parseInt(pathInfo);
			res = pathInfo;
		}

		return res;
	}

	private String inputStreamToString(InputStream inputStream) {		
		Scanner scanner = new Scanner(inputStream, "UTF-8");
		return scanner.hasNext() ? scanner.useDelimiter("\\A").next() : "";
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("ValidationReportStatus.doGet(): init");
		/*
		 * String param = ""; param = request.getParameter("msg");
		 */
		String mcappId = retrieveMCAppId(request);

		System.out.println("ValidationReportStatus.doGet(): mcappId=." + mcappId + ".");
		if (mcappId != null) {
			response.setContentType("text/event-stream");
			response.setCharacterEncoding("UTF-8");
			// response.setHeader("Connection", "keep-alive"); Necesario???
			response.setHeader("Cache-Control", "no-cache");
			PrintWriter writer = response.getWriter();

			ManageMCApp ME = new ManageMCApp(factory);
			int cont = 0;
			while (cont >= 0 && cont < Cycle) {
				MCApp obj = ME.getMCApp(mcappId);
				Integer statusCamelReport = -1;
				if (obj != null)
					statusCamelReport = obj.getStatusCamelReport(); // OJO: si
																	// no existe
																	// obj
																	// statusCamelReport
																	// == -1				
				if (statusCamelReport == 0) {
					// writer.write("event:OK\n");
					writer.write("data: OK-" + mcappId + "\n\n"); // podria
																	// enviar
																	// numero de
																	// errores o
																	// la
																	// lista!!!
					writer.flush();
					cont = -1;
				} else if (statusCamelReport == 2) {
					// writer.write("event:OK\n");
					writer.write("data: DB-" + mcappId + "\n\n"); // podria
																	// enviar
																	// numero de
																	// errores o
																	// la
																	// lista!!!
					writer.flush();
					cont = -1;
				} else {
					try {
						Thread.sleep(1000);
						cont++;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} // while (cont) */
			if (cont >= Cycle) {
				// writer.write("event:UNKNOWN\n");
				writer.write("data: UNKNOWN-" + mcappId + "\n\n");
				writer.flush();
			}
			writer.close();

		} // if ( mcappId != null ) {
		System.out.println("ValidationReportStatus.doGet(): end");
	}

	@Override
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("ValidationReportStatus.doPut(): init");
		String res = "false";

		String mcappId = retrieveMCAppId(request);
		String body = inputStreamToString(request.getInputStream());
		// for example "{"status":1}"
		int beginIndex = body.lastIndexOf("\"status\":");
		int begin2 = beginIndex + "\"status\":".length();
		System.out.println("ValidationReportStatus.doPut(): begin2=." + begin2);
		String statusCamelReport = body.substring(begin2, begin2 + 1);
		
		if (mcappId != null && statusCamelReport != null) {
			ManageMCApp ME = new ManageMCApp(factory);
			ME.updateStatusMCApp(Long.valueOf(mcappId), new Integer(statusCamelReport));
			MCApp obj = ME.getMCApp(mcappId);
			if (obj != null)
				statusCamelReport = Integer.toString(obj.getStatusCamelReport());
			else
				statusCamelReport = "" + -1; // OJO: si no existe obj
												// statusCamelReport == -1			
		} // if ( param != null && param.length() > 0 ) {
		System.out.println("ValidationReportStatus.doPut(): end");
	}
}
