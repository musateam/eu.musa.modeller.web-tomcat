/*******************************************************************************
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.camel_dsl.dsl.web

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.web.server.IServiceContext
import org.eclipse.xtext.web.server.model.IWebResourceSetProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.util.StringInputStream
import java.io.FileInputStream
import java.util.Scanner


//MCP
/**
 * Resources that run in multi-resource mode share the same resource set.
 */
class CamelDslResourceSetProvider implements IWebResourceSetProvider {
	
	static val MULTI_RESOURCE_PREFIX = 'multi-resource'
		
	@Inject Provider<ResourceSet> provider

	private static final String fileExtension = ".camel";
	private static final String UTF_8 = "UTF-8";
	
	override get(String resourceId, IServiceContext serviceContext) {
		/* MCP
		if (resourceId !== null && resourceId.startsWith(MULTI_RESOURCE_PREFIX)) {
			val pathEnd = Math.max(resourceId.indexOf('/'), MULTI_RESOURCE_PREFIX.length)
			return serviceContext.session.get(ResourceSet -> resourceId.substring(0, pathEnd), [provider.get])
		} else
			return provider.get
 
		*/
		//MCP
		val result = if (resourceId !== null && resourceId.startsWith(MULTI_RESOURCE_PREFIX)) {
			val pathEnd = Math.max(resourceId.indexOf('/'), MULTI_RESOURCE_PREFIX.length)
			serviceContext.session.get(ResourceSet -> resourceId.substring(0, pathEnd), [provider.get])
		} else
			provider.get
			
		result => [
			System.err.println("1111")
			//val String resourceBaseProvider = 'D:/MUSA/test-files'
			//val String resourceBaseProviderDB = 'D:/MUSA/db-files'
	        //var varResourceBaseProvider = '/home/tecnalia/MUSA/test-files'
			var varResourceBaseProviderDB = '/home/MUSA/MODELLER/db-files'
	        if (System.getProperty("os.name").contains("Windows")){
	        	//varResourceBaseProvider = 'D:/MUSA/test-files'
				varResourceBaseProviderDB = 'D:/MUSA/db-files'        	
	        } 
	        //val resourceBaseProvider = varResourceBaseProvider
			val resourceBaseProviderDB = varResourceBaseProviderDB       

			val ry = getResource(URI.createURI(resourceBaseProviderDB+"/MUSAAGENTS"+fileExtension),false) 

			if(ry != null) // nunca se da este caso!!!
			{ 
			    System.err.println("URI=" + ry.toString());
			    ry.load(null);
			}			
			if (ry == null) {
				val fileStream = new FileInputStream(resourceBaseProviderDB+"/MUSAAGENTS"+fileExtension);			            
	            val _text = new Scanner(fileStream, UTF_8).useDelimiter("\\A").next();
				fileStream.close();				
				
				val r = createResource(URI.createURI(resourceBaseProviderDB+"/MUSAAGENTS.camel"))
				if(r != null) System.err.println("r=" + r.toString());
				
				/* 
				val in = new StringInputStream('''
				camel model MUSA {
					deployment model MUSADEP {
						internal musa agent component ENFAG1
						internal musa agent component ENFAG2 
					    internal musa agent component ENFAG3
					}	
				}
				'''.toString)
				*/
				val in = new StringInputStream(''+_text+'')			
				r.load(in, null)
				println(r.contents+".2222")
			}
		]	
		result
		
		//MCP		
	}
	
}
